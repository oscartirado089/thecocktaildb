function buscarCoctel() {
    const coctelInput = document.getElementById('txtNombre').value;

    if (coctelInput.trim() !== '') {
        const apiUrl = `https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${coctelInput}`;
        
        axios.get(apiUrl)
            .then(response => {
                if (response.data && response.data.drinks && response.data.drinks.length > 0) {
                    const coctel = response.data.drinks[0];

                    if (coctel.strDrink.toLowerCase() === coctelInput.toLowerCase()) {
                        mostrarInfoCoctel(coctel);
                    } else {
                        alert('Coctel no encontrado. Intenta con otro nombre.');
                    }
                } else {
                    alert('Coctel no encontrado. Intenta con otro nombre.');
                }
            })
            .catch(error => {
                console.error('Error al obtener datos del API', error);
            });
    } else {
        alert('Por favor, ingresa el nombre del coctel');
    }
}

function mostrarInfoCoctel(coctel) {
    const coctelNombre = document.getElementById('coctelNombre');
    const coctelCategoria = document.getElementById('coctelCategoria');
    const coctelInstrucciones = document.getElementById('coctelInstrucciones');
    const coctelImagen = document.getElementById('coctelImagen');
    const coctelInfo = document.getElementById('coctelInfo');


    coctelImagen.style.maxWidth = '1200px';
    coctelImagen.style.maxHeight = '400px';
    coctelImagen.src = coctel.strDrinkThumb;
    coctelInfo.classList.remove('hidden');
    coctelNombre.textContent = coctel.strDrink;
    coctelCategoria.textContent = coctel.strCategory;
    coctelInstrucciones.textContent = coctel.strInstructions;
    coctelImagen.src = coctel.strDrinkThumb;

    coctelInfo.classList.remove('hidden');
}

function limpiarContenido() {
    document.getElementById('coctelNombre').innerHTML = '';
    document.getElementById('coctelCategoria').innerHTML = '';
    document.getElementById('coctelInstrucciones').innerHTML = '';
    document.getElementById('coctelImagen').src = '';
}